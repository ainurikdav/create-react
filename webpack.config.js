// Node.js использует модульную систему
// Для загрузки модулей применяется функция require()
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = (env = {}) => {
  console.log(env)
  const { mode = 'production'} = env;
  const isProd = mode === 'production';
  const isDev = mode === 'development';

  const getStyleLoaders = () => {
    return [
      isProd ? MiniCssExtractPlugin.loader : 'style-loader'
    ]
  }

  const getPlugins = () => {
    const plugins = [
      new HtmlWebpackPlugin({
        buildTime: new Date().toISOString(),
        template: "./src/index.html"
      }),
    ]
    
    if(isProd) {
      plugins.push( new MiniCssExtractPlugin({
        filename: 'main-[hash:8].css'
      })
      )
    }

    return plugins;
  }

  return {
    mode: isProd ? 'production': isDev && 'development',
    entry: "./src/index.js",
    output: {
    path: path.join(__dirname, "/dist"),
    filename: "index-bundle.js"
  },
  module: {
    // webpack обрабатывает файлы при помощи loader`ов
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ["babel-loader"]
      },
      {
        test: /\.(css|scss)$/,
        use: [...getStyleLoaders(), 'css-loader',
              'sass-loader']
      },
      {
        test: /\.(png|jpg|gif|ico|jpeg)$/,
        use: [{loader: 'file-loader', 
               options: {
                outputPath: 'images',
                name: '[name]-[sha1:hash:7].[ext],'
              }
         }
        ] 
      },
      {
        test: /\.(ttf|otf|eot|woff|woff2)$/,
        use: [{loader: 'file-loader', 
               options: {
                outputPath: 'fonts',
                name: '[name].[ext],'
              }
         }
        ] 
      }
    ]
  },
  
  // библиотека, помогает найти модуль по абсолютному пути
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },

  // Расширяют возможности loaders (имеют доступ ко всем ресурсам, создаваемые после упаковки)
  // Применяется для изменения конфигурации сборки, оптимизации 
  plugins: getPlugins()
  }
  
};