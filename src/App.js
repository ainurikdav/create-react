import React, { Component } from "react";

import './App.css';

import MainPage from './components/MainPage/MainPage';

class App extends Component {
    render() {
        return (
            <React.Fragment>
                <MainPage/>
            </React.Fragment>
        );
    }
}

export default App;