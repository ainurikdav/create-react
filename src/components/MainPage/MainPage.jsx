import React, {Component} from  "react";

import './MainPage.scss';

import logo512 from '../../../assets/logo512.png'

class MainPage extends Component {
    render() {
        return(
            <div className='Main-page Main-page__root'>
                <h1>Ай нурым</h1>
                <img src={logo512}/>
            </div>
        );
    }
}

export default MainPage;